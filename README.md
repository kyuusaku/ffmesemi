# Code for reproduction of the paper:

Accelerating Flexible Manifold Embedding for Scalable Semi-Supervised Learning

# Our test is on:

* Matlab2014a
* Ubuntu 16.04

# Project structure:

* all the data used in our paper can be download by [Baidu Yun](https://pan.baidu.com/s/1mebXNVKNpfeCCI6xyDzJpw)
* `main.m` is for reproduction
