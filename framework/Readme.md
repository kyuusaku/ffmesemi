Adding a new dataset should modify the following files:
* `parse_inputs.m`
* `get_data_name.m`
* `load_dataset.m`